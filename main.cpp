#include <iostream>
#include <array>
#include <string>
#include <algorithm>
#include <typeinfo>

template<typename T, size_t size>
std::string array_to_string(const std::array<T, size>& arr)
{
    std::string res = "(";

    for (auto i : arr){
        res.append(std::to_string(i)).append(", ");
    }

    res.erase(res.end()-2, res.end());

    res += ")";

    return res;
}

template<typename T, size_t size>
T sum(const std::array<T, size>& arr)
{
    T res = 0;

    for (auto i : arr)
    {
        res += i;
    }

    return res;
}

template<typename T, size_t size>
T average(const std::array<T, size>& arr)
{
    return T(sum(arr)/arr.size());
}

int main(int argc, char *argv[])
{
    std::array<int, size_t(5)> arr = {10, 12, 25, 33, 45};

    std::cout << "Оригинал: " << array_to_string(arr) << std::endl;

    std::sort(arr.begin(), arr.end());

    std::cout << "Сортировка: " << array_to_string(arr) << std::endl;

    std::cout << "Доступ к элементу с помощью \".at()\": " << arr.at(1) << std::endl;
    
    std::cout << "Доступ к элементу с помощью \"operator []\": " << arr[2] << std::endl;

    std::cout << "Сумма элементов массива : " << sum(arr) << std::endl;

    std::cout << "Среднее значение элементов массива : " << average(arr) << std::endl;

    std::cout << "Найти позицию элемента \"11\" в массиве: " << std::distance(arr.begin(), std::find(arr.begin(), arr.end(), 11)) << std::endl;
    
    std::replace(arr.begin(), arr.end(), 25, 99);

    std::cout << "Заменить элемент \" 25\"  на \" 99 \" в массиве: " << array_to_string(arr) << std::endl;
}
